<%@ tag pageEncoding="UTF-8" import="com.cloud.platform.*" %>

<%@ attribute name="extend" %>

<%
	if("xls".equals(extend) || "xlsx".equals(extend)) {
		out.print("excel_bk");
	}
	else if("doc".equals(extend) || "docx".equals(extend)) {
		out.print("doc_bk");
	}
	else if("ppt".equals(extend) || "pptx".equals(extend)) {
		out.print("ppt_bk");
	}
	else if("pdf".equals(extend)) {
		out.print("pdf_bk");
	}
	else if(DocConstants.isText(extend)) {
		out.print("txt_bk");
	}
	else if(Constants.isImage(extend)) {
		out.print("");
	}
	else {
		out.print("file_bk");
	}
%>