package com.cloud.doc.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.cloud.platform.Tree;

@Entity
@Table(name = "t_doc_dir")
public class Directory extends Tree {

	public Directory() {}
	
	public Directory(String id) {
		super.setId(id);
	}
}
